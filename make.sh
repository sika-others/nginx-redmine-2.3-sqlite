#!/bin/bash

PACKAGE=redmine-nginx-2.3-sqlite.deb

[ -d build ] || mkdir build

dpkg-deb --build src $PACKAGE

mv $PACKAGE build

